# Dali Clock for Android, Copyright (c) 2015 by Robin Müller-Cajar.

TITLE		= Dali Clock
TARGET		= DaliClock
TARGET2		= daliclock
VENDOR		= Jamie Zawinski (Robin Müller-Cajar)
VERSION		= $(shell sed -n \
			's/^.*\([0-9][0-9]*\.[0-9][0-9]*\).*$$/\1/p' \
			< ../version.h)
VERSION_NUM	=  $(shell sed -n \
			's/^.*\([0-9][0-9]*\)\.\([0-9][0-9]*\).*$$/\1\2/p' \
			< ../version.h)
TOOLS_VERSION	= $(shell sed -n \
			's/^.*buildToolsVersion."\([^"]*\)".*$$/\1/p' \
			< $(TARGET)/app/build.gradle)
ID		= org.jwz.$(TARGET2)
APK		= $(TARGET)-$(VERSION_NUM).apk
CERT		= my-keystore.keystore

CC		= gcc
CFLAGS		= -g -Wall -Wstrict-prototypes -Wnested-externs
LDFLAGS		= 
DEFINES		= -DBUILTIN_FONTS
INCLUDES	= -I. -I../font -I../OSX
OBJS		= buildnumbers.o

FONTDIR		= $(TARGET)/app/src/main/res/raw
NUMBERS		= $(FONTDIR)/font0.json \
		  $(FONTDIR)/font1.json \
		  $(FONTDIR)/font2.json \
		  $(FONTDIR)/font3.json \
		  $(FONTDIR)/font4.json \
		  $(FONTDIR)/font5.json \
		  $(FONTDIR)/font6.json \
		  $(FONTDIR)/font7.json \

default:: release
all: debug release

clean::
	rm -f *.o buildnumbers

gradle_clean::
	cd $(TARGET); ./gradlew clean

distclean:: clean gradle_clean
	rm -rf $(TARGET)/build
	rm -f  $(TARGET)/local.properties
	rm -f  $(TARGET)/.idea/workspace.xml
	rm -rf $(TARGET)/.idea/libraries
	rm -rf $(TARGET)/.gradle
	rm -f  $(TARGET)/app/manifest-merger-release-report.txt
	rm -rf $(TARGET)/app/libs
	rm -rf sign-*


distdepend:: update_gradle_version update_gradle_app_id

echo_tarfiles:
	@echo `find . \
	  \( \( -name '*~*' -o -name '*.o' \
		-o -name buildnumbers -o -name '*.apk' \
		-o -name '*report.txt' \
		-o -name 'local.properties' \
		-o -name 'workspace.xml' \
		-o -name '.gradle' \
		-o -name 'libraries' \
		-o -name '.DS_Store' \
		-o -name 'build' \
		-o -name '*.keystore' \
	      \) \
	     -prune \) \
	  -o -type f -print \
	| sed 's@^\./@@' \
	| sort`


buildnumbers.o:
	$(CC) -c $(INCLUDES) $(DEFINES) $(CFLAGS) ../font/buildnumbers.c

buildnumbers: buildnumbers.o
	$(CC) $(LDFLAGS) -o $@ $(OBJS)

$(NUMBERS): buildnumbers

$(FONTDIR)/font0.json:
	./buildnumbers 0 > $@
$(FONTDIR)/font1.json:
	./buildnumbers 1 > $@
$(FONTDIR)/font2.json:
	./buildnumbers 2 > $@
$(FONTDIR)/font3.json:
	./buildnumbers 3 > $@
$(FONTDIR)/font4.json:
	./buildnumbers 4 > $@
$(FONTDIR)/font5.json:
	./buildnumbers 5 > $@
$(FONTDIR)/font6.json:
	./buildnumbers 6 > $@
$(FONTDIR)/font7.json:
	./buildnumbers 7 > $@

_deps:: update_gradle_version update_gradle_app_id check_android_home
debug::   $(NUMBERS) ../version.h _deps
release:: $(NUMBERS) ../version.h _deps
debug::
	cd $(TARGET) && ./gradlew assembleDebug
release::
	cd $(TARGET) && ./gradlew assembleRelease


KEYSTORE = my-keystore.keystore
$(KEYSTORE):
	keytool -genkey -v -keystore $@ \
	-alias daliclock -keyalg RSA -keysize 2048 -validity 10000

APK_DIR       = daliclock/app/build/outputs/apk/release/
APK_UNSIGNED  = $(APK_DIR)app-release-unsigned.apk
APK_SIGNED    = $(APK_DIR)app-release.apk

  TOOLDIR = $(shell ls -d $$HOME/Library/Android/sdk/build-tools/* | tail -1)
 ZIPALIGN = $(TOOLDIR)/zipalign
APKSIGNER = $(TOOLDIR)/apksigner

sign_release::
	rm -f $(APK_SIGNED)
	$(ZIPALIGN) -v 4 $(APK_UNSIGNED) $(APK_SIGNED)
	$(APKSIGNER) sign --ks $(KEYSTORE) $(APK_SIGNED)
	$(APKSIGNER) verify $(APK_SIGNED)
	@ls -lF $(APK_SIGNED)
	stty -echo

apk:: release
	@\
  SRC=../version.h ;							    \
  VERS=`sed -n 's/[^0-9]*\([0-9]\.[0-9][^. \"]*\).*/\1/p' < $$SRC |	    \
	sed 's/\.//g' | head -1`;					    \
  HEAD="DaliClock-$$VERS" ;						    \
  if [ ! -s $(APK_SIGNED) -o $(APK_UNSIGNED) -nt $(APK_SIGNED) ]; then	\
    $(MAKE) sign_release ;						\
  fi ;									\
  set -x ;								\
  cp -p $(APK_SIGNED) ../archive/$$HEAD.apk


check_android_home:
  ifndef ANDROID_HOME
    ANDROID_HOME    := $(shell sed -n \
			's/^.*sdk\.dir.\(.*\)[ ]*$$/\1\//p' \
			< $(TARGET)/local.properties)
  endif
  ifeq ($(strip $(ANDROID_HOME)),)
    $(error Cannot find the Android SDK - Please set the ANDROID_HOME variable to your android-sdk directory) 
  endif

update_gradle_app_id:
	@S="$(TARGET)/app/build.gradle" ;				\
  /bin/echo -n "Updating application id in $$S to $(ID)... " ;		\
  T=/tmp/xs.$$$$ ;							\
  sed -e "s/^\(.*applicationId.*\"\).*\(\".*\)$$/\1$(ID)\2/"	      	\
     < $$S > $$T ;							\
  if cmp -s $$S $$T ; then						\
    echo "unchanged." ;							\
  else									\
    cat $$T > $$S ;							\
    echo "done." ;							\
  fi ;									\
  rm $$T

update_gradle_version:
	@S="$(TARGET)/app/build.gradle" ;				\
  DATE=`date +%d-%h-%Y` ;						\
  NAME="${VERSION}, $$DATE" ;						\
  CODE="${VERSION_NUM}" ;						\
  /bin/echo -n "Updating version in $$S to $$CODE, \"$$NAME\"... " ;	\
  T=/tmp/xs.$$$$ ;							\
  sed -e "s/^\(.*versionCode\).*/\1 $$CODE/"				\
      -e "s/^\(.*versionName\).*/\1 \"$$NAME\"/"			\
     < $$S > $$T ;							\
  if cmp -s $$S $$T ; then						\
    echo "unchanged." ;							\
  else									\
    cat $$T > $$S ;							\
    echo "done." ;							\
  fi ;									\
  rm $$T


tail_log:
	$(ANDROID_HOME)/platform-tools/adb logcat -v tag 'DaliClock:V' '*:S'

// Dali Clock - a melting digital clock for the PalmOS.
// Copyright (c) 1991-2023 Jamie Zawinski <jwz@jwz.org>
//
// Permission to use, copy, modify, distribute, and sell this software and its
// documentation for any purpose is hereby granted without fee, provided that
// the above copyright notice appear in all copies and that both that
// copyright notice and this permission notice appear in supporting
// documentation.  No representations are made about the suitability of this
// software for any purpose.  It is provided "as is" without express or
// implied warranty.

#include "daliclock.h"

VERSION 1       "2.48"
ICONFAMILY      "icon-32-2.pbm" "" "" "icon-32-8.ppm" TRANSPARENTINDEX 0
SMALLICONFAMILY "icon-16-2.pbm" "" "" "icon-16-8.ppm" TRANSPARENTINDEX 0


FORM ID MainForm AT (0 0 FORM_WIDTH FORM_HEIGHT)
USABLE
MENUID MainMenu
BEGIN
	TITLE "Dali Clock"
END

FORM ID AboutForm AT (2 2 FORM_WIDTH-4 FORM_HEIGHT-4)
MODAL
USABLE
BEGIN
	TITLE "About..."

	LABEL "Dali Clock" AUTOID AT (CENTER 30) FONT 2
	LABEL "2.48" AUTOID AT (CENTER PREVBOTTOM) FONT 2
        LABEL "by" AUTOID AT (CENTER PREVBOTTOM + 6)
        LABEL "Jamie Zawinski <jwz@jwz.org>" AUTOID AT (CENTER PREVBOTTOM)
        LABEL "Copyright (c) 1991-2023" AUTOID AT (CENTER PREVBOTTOM)
        LABEL "http://www.jwz.org/xdaliclock/"
                AUTOID AT (CENTER PREVBOTTOM + 6)
 
        BUTTON "OK" AUTOID AT (CENTER 140 AUTO AUTO) FONT 0 USABLE
END

FORM ID SettingsForm AT (2 2 FORM_WIDTH-4 FORM_HEIGHT-4)
MODAL
USABLE
BEGIN
	TITLE "Dali Clock"
        BUTTON "OK" AUTOID AT (CENTER 140 AUTO AUTO) FONT 0 USABLE

        LABEL "Background:" AUTOID AT (RIGHT@FORM_CENTER 15)
        CHECKBOX "dark" ID SettingsButtonDark
                AT (PrevRight + 4 PrevTop AUTO AUTO)
                GROUP 1
        CHECKBOX "light" ID SettingsButtonLight
                AT (PrevLeft PrevBottom AUTO AUTO)
                GROUP 1

        LABEL "Time Mode:" AUTOID
                AT (RIGHT@FORM_CENTER PrevBottom + 4)
        CHECKBOX "12 hour" ID SettingsButton12
                AT (PrevRight + 4 PrevTop AUTO AUTO)
                GROUP 2
        CHECKBOX "24 hour" ID SettingsButton24
                AT (PrevLeft PrevBottom AUTO AUTO)
                GROUP 2
        CHECKBOX "seconds only" ID SettingsSecondsOnly
                AT (PrevLeft PrevBottom AUTO AUTO)
                GROUP 2

        LABEL "Date Format:" AUTOID
                AT (RIGHT@FORM_CENTER PrevBottom + 4)
        CHECKBOX "MM-DD-YY" ID SettingsMMDDYY
                AT (PrevRight + 4 PrevTop AUTO AUTO)
                GROUP 3
        CHECKBOX "DD-MM-YY" ID SettingsDDMMYY
                AT (PrevLeft PrevBottom AUTO AUTO)
                GROUP 3
        CHECKBOX "YY-MM-DD" ID SettingsYYMMDD
                AT (PrevLeft PrevBottom AUTO AUTO)
                GROUP 3

        LABEL "Frames/Second:" AUTOID
                AT (RIGHT@FORM_CENTER PrevBottom + 4)
        FIELD ID SettingsFieldFPS AT (PrevRight + 4 PrevTop 20 PrevBottom)
                EDITABLE UNDERLINED SINGLELINE MAXCHARS 3 NUMERIC

END

ALERT ID RomIncompatibleAlert
ERROR
BEGIN
        TITLE "System Incompatible"
        MESSAGE "System Version 2.0 or greater is required to run " \
         "this application."
        BUTTONS "OK"
END


MENU ID MainMenu
BEGIN
	PULLDOWN "Options"
	BEGIN
		MENUITEM "About..."    MainMenuAbout    "A"
		MENUITEM "Settings..." MainMenuSettings "S"
	END
END

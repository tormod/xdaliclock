/* xdaliclock - a melting digital clock
* Copyright (c) 1991-2006 Jamie Zawinski <jwz@jwz.org>
*
* Permission to use, copy, modify, distribute, and sell this software and its
* documentation for any purpose is hereby granted without fee, provided that
* the above copyright notice appear in all copies and that both that
* copyright notice and this permission notice appear in supporting
* documentation.  No representations are made about the suitability of this
* software for any purpose.  It is provided "as is" without express or
* implied warranty.
*/

#import "DaliClockWindow.h"

@implementation DaliClockWindow

/* Normally borderless windows don't accept keyboard input.
   The only reason this subclass exists is to change this.
 */
- (BOOL)canBecomeKeyWindow
{
  return YES;
}


/* Called when a menu is popped down or when a keyboard equivalent is
   typed to determine whether that menu item is runnable.  We override
   this to make Close/Zoom/Minimize be enabled even when the window
   doesn't have those controls in the title bar because it is borderless.
 */
- (BOOL)validateMenuItem:(id)item
{
  SEL action = [item action];
  const char *name = sel_getName(action);
  if (!strcmp(name, "performClose:") ||         // handled here
      !strcmp(name, "performZoom:") ||          // handled here
      !strcmp(name, "performMiniaturize:")) {   // handled in AppController
    return YES;
  } else {
    return [super validateMenuItem:item];
  }
}


/* "Window / Close".
  We must implement this ourselves because the NSWindow version beeps
  when the window is borderless (and therefore has no "minimize" button).
 */
- (void)performClose:(id)sender
{
  [self close];
}

#if 0
/* "Window / Zoom", via FirstResponder.
  We must implement this ourselves because the NSWindow version beeps
  when the window is borderless (and therefore has no "minimize" button).
 */
- (void)performZoom:(id)sender
{
  printf("ZOOOM\n");
  [self zoom:sender];       // #### does nothing when borderless
}
#endif


/* This doesn't make the "Zoom" menu item be enabled for borderless windows...
 */
- (BOOL)windowShouldZoom:(NSWindow *)sender toFrame:(NSRect)newFrame
{
  return YES;
}

@end
